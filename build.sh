#!/bin/bash

wget -O mdbook.tar.gz "https://github.com/rust-lang/mdBook/releases/download/v0.4.12/mdbook-v0.4.12-x86_64-unknown-linux-gnu.tar.gz"
tar -zxvf mdbook.tar.gz -C . && chmod a+x mdbook && ./mdbook build
